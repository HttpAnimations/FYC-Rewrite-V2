const { contextBridge, shell } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
    openExternal: shell.openExternal
});

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('a[href]').forEach(link => {
    const url = link.href;
    if (url.startsWith('http://') || url.startsWith('https://')) {
      link.addEventListener('click', (e) => {
        e.preventDefault();
        shell.openExternal(url);
      });
    }
  });
});
