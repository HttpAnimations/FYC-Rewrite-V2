# README
FYC is now on gitlab :3

# FYC-Rewrite-V2/Stable
This branch holds the source code of FYC.

## Installing
To install open a terminal and run the following commands.

```bash
git clone -b Stable https://gitlab.com/HttpAnimations/FYC-Rewrite-V2.git
```

```bash
cd FYC-Rewrite-V2
```

```
npm start
```

## App (Optiional)

```
npm run make
```